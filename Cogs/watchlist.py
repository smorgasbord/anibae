import discord
from discord.ext import commands
import time
import urllib3


class WatchlistCog(commands.Cog, name="watchlist command"):
	def __init__(self, bot, httpClient):
		self.bot = bot
		self.httpClient = httpClient
        
	@commands.command(name = "watchlist",
					usage="",
					description = "See your watchlist.")
	@commands.cooldown(1, 2, commands.BucketType.member)
	async def watchlist(self, ctx):
		r = self.httpClient.request('GET', 'https://api.jikan.moe/v3/user/aquafemi/animelist/all')

		print(r.status, r.status)
		print(r.data, r.data)
		message = await ctx.send(r.data)

def setup(bot):
	bot.add_cog(WatchlistCog(bot, urllib3.PoolManager()))